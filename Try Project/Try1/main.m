//
//  main.m
//  Try1
//
//  Created by Shravan on 10/20/2013.
//  Copyright (c) 2013 Shravan. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AwenAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AwenAppDelegate class]));
    }
}
