//
//  AwenAppDelegate.h
//  Try1
//
//  Created by Shravan on 10/20/2013.
//  Copyright (c) 2013 Shravan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AwenAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
