//
//  WhereamiViewControllerCopy.m
//  Whereami
//
//  Created by Shravan on 11/6/2013.
//
//

#import "WhereamiViewControllerCopy.h"

@implementation WhereamiViewControllerCopy

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil
								  bundle:nibBundleOrNil];
	if (self)
	{
		//create location manager object
		locationManager = [[CLLocationManager alloc]init];
		
		[locationManager setDelegate:self];
		
		[locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
		
		[locationManager startUpdatingLocation];
		
		
	}
	
	return self;
	
}

-(void) locationManager:(CLLocationManager *)manager
	 didUpdateToLocation:(CLLocation *)newLocation
			  fromLocation:(CLLocation *)oldLocation
{
	NSLog(@"%@",newLocation);
}

-(void) locationManager:(CLLocationManager *) manager
		 didFailWithError:(NSError *)error
{
	NSLog(@"could not something %@",error);
}


- (void)dealloc
{
	[locationManager setDelegate:nil];
}


@end
