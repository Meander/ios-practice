//
//  BNRMapPoint.h
//  Whereami
//
//  Created by Shravan on 11/7/2013.
//
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface BNRMapPoint : NSObject <MKAnnotation>
{
	
}
-(id) initWithCoordinate:(CLLocationCoordinate2D) coord title:(NSString*) t;

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

@property (nonatomic, copy) NSString *title;

@end
