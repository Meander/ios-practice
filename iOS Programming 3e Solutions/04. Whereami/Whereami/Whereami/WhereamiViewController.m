//
//  WhereamiViewController.m
//  Whereami
//
//  Created by joeconway on 7/31/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import "WhereamiViewController.h"


@interface WhereamiViewController()


@end



@implementation WhereamiViewController


//@synthesize guiMapTypeSelectorSegmentedControl;

-(void)viewDidLoad
{
	[uiMainMapView setShowsUserLocation:YES];
}



//mapview
- (void) mapView:(MKMapView *)mapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
	CLLocationCoordinate2D loc = [userLocation coordinate];
	
	MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(loc, 250, 250);
	[mapView setRegion:region animated:YES];
}


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if(self) {
        // Create location manager object
        locationManager = [[CLLocationManager alloc] init];
        
        // There will be a warning from this line of code; ignore it for now
        [locationManager setDelegate:self];

        // And we want it to be as accurate as possible
        // regardless of how much time/power it takes
        [locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
		 
		 
        // Tell our manager to start looking for its location immediately
        //[locationManager startUpdatingLocation];
		 
		 //[locationManager startUpdatingHeading];
		 
		 [guiMapTypeSelectorSegmentedControl addTarget:self
															 action:@selector(changeMapType:)
												forControlEvents:UIControlEventValueChanged];
		 
		 
		 
		 locationTitleField.delegate = self;
    }
    return self;
}



- (IBAction)guiMaptypeSelection:(id)sender
{
	[self changeMapType];
}

-(void) changeMapType
{
	if ([guiMapTypeSelectorSegmentedControl selectedSegmentIndex] == 0)
	{
		[uiMainMapView setMapType:MKMapTypeHybrid];
	}
	else
	{
		[uiMainMapView setMapType:MKMapTypeStandard];
	}
}

//location manager
- (void)locationManager:(CLLocationManager *)manager
    didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
	
	CLLocationDistance tempDistance =  [newLocation distanceFromLocation:oldLocation];
	
	if (tempDistance > 500 || newLocation == nil || oldLocation == nil )
	{
		NSTimeInterval t = [[newLocation timestamp] timeIntervalSinceNow];
		
		if (t < -180)
		{
			return;
		}
	
		[self foundLocation:newLocation];
	
	}
}

- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
	
    NSLog(@"Could not find location: %@", error);
}




- (void) locationManager:(CLLocationManager *)manager
		  didUpdateHeading:(CLHeading *)newHeading
{
	_phoneHeading = nil;
	
	_phoneHeading = [newHeading copy];
	
}

- (void)findLocation
{
	
	[uiMainMapView setMapType:MKMapTypeHybrid];
	[locationManager startUpdatingLocation];
	[activityIndicator startAnimating];
	[locationTitleField setHidden:YES];
}
- (void)foundLocation:(CLLocation *)loc
{
	CLLocationCoordinate2D coord = [loc coordinate];
	// Create an instance of BNRMapPoint with the current data
	BNRMapPoint *mp = [[BNRMapPoint alloc]
							 initWithCoordinate:coord
							 title:
							 [locationTitleField text]];
	// Add it to the map view
	[uiMainMapView	addAnnotation:mp];
	// Zoom the region to this location
	MKCoordinateRegion region =
	MKCoordinateRegionMakeWithDistance(coord, 250, 250);
	[uiMainMapView setRegion:region animated:YES];
	// Reset the UI
	[locationTitleField setText:@""];
	[activityIndicator stopAnimating];
	[locationTitleField setHidden:NO];
	[locationManager stopUpdatingLocation];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
	// This method isn't implemented yet - but will be soon.
	[self findLocation];
	[textField resignFirstResponder];
	return YES;
}




- (void)dealloc
{
    // Tell the location manager to stop sending us messages
    [locationManager setDelegate:nil];
}
@end
