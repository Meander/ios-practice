//
//  WhereamiViewController.h
//  Whereami
//
//  Created by joeconway on 7/31/11.
//  Copyright 2011 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

#import "BNRMapPoint.h"

@interface WhereamiViewController : UIViewController
    <CLLocationManagerDelegate, MKMapViewDelegate,
		UITextFieldDelegate>
{
    CLLocationManager *locationManager;
	IBOutlet MKMapView *uiMainMapView;
	IBOutlet UIActivityIndicatorView *activityIndicator;
	IBOutlet UITextField *locationTitleField;
	
	
	__weak IBOutlet UISegmentedControl *guiMapTypeSelectorSegmentedControl;
	
	
}

@property (nonatomic, strong)  CLHeading *phoneHeading;

- (IBAction)guiMaptypeSelection:(id)sender;
@end
