//
//  BNRMapPoint.m
//  Whereami
//
//  Created by Shravan on 11/7/2013.
//
//

#import "BNRMapPoint.h"

@implementation BNRMapPoint

@synthesize coordinate, title;


-(id) initWithCoordinate:(CLLocationCoordinate2D)c title:(NSString *)t
{
	self = [super init];
	if (self)
	{
		coordinate = c;
		[self setTitle:t];
	}
	
	return self;
}

-(id) init
{
	return [self initWithCoordinate:CLLocationCoordinate2DMake(40,-89) title:@"Home"];
}


@end
