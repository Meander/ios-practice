//
//  main.m
//  PhotoOrganizer
//
//  Created by Shravan on 11/9/2013.
//  Copyright (c) 2013 Shravan. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AWNAppDelegate.h"

int main(int argc, char * argv[])
{
	@autoreleasepool {
	    return UIApplicationMain(argc, argv, nil, NSStringFromClass([AWNAppDelegate class]));
	}
}
